# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 0.2.8
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* To set this up run the following command:

    ```
    pip install symbolic-quantum-computation
    ```

    To update:

    ```
    pip install --upgrade symbolic-quantum-computation
    ```

    To run use:

    ```
    sqc
    /* ENTER */
    EXP
    ```
    EXP - expression. It must be without spaces and quotes.

    or

    ```
    sqc --exp EXP
    ```
    EXP - expression. It must be without spaces, with quotes

    Example:
    
    ```
    sqc --exp 'a+b'
    ```


    To run from file use:

    ```
    sqc --f FILE
    ```

    FILE - path to the file. You can write a lot of expressions, one in line.
    Expressions must be without quotes and spaces

    Example:
    
    ```
    sqc --f 'input.txt'
    ```


    To run advanced mode use:

    ```
    sqc -m
    ```

    Examples:

    ```
    sqc -m
    ```

    ```
    sqc -m --exp 'a+b'
    ```

    ```
    sqc -m --f 'input.txt'
    ```

    The syntax of advanced mode:

        ```
        > exp
        ```

        ```
        > rule
        i. new_exp
        ```

        where:
            i is the number of alternative
            new_exp - new expression with current rule


    Step-by-step solution writes at log.txt file

    REPL realized

* Configuration
* Dependencies
* Database configuration
* To run tests use:

    ```
    sqc -t
    ```

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

* About AST

    Для реализации сокращения формулы мы будем использовать модель абстрактного синтаксического дерева
в библиотеки ast.py

    Нам понадобятся метод ast.parse() - позволяет преобразовать строку в дерево

* Список операций, которые распознаёт библиотека ast:

    * Сложение +  Add
    * Вычитание - Sub
    * Умножение * Mult
    * Деление / Div
    * Взятие модуля % Mod
    * Возведение в степень ** Pow
    * Битовый сдвиг влево << LShift
    * Битовый сдвиг вправо >> RShift
    * Битовое Или | BitOr
    * Битовое ИСКЛЮЧАЮЩЕЕ ИЛИ ^ BitXor
    * Битовое И & BitAnd
    * Двойное деление // FloorDiv
    * Битовое инвертирование (1->0, 0->1) ~ Invert
    * Логическое Не not Not
    * Унарное сложение + UAdd
    * Унарное вычитание - USub

### Поддерживаемые операции: ###
* Унарные:
    * \+
    * \-
    * sin
    * cos
    * tg
    * ctg
    * exp
    * abs
    * arcsin
    * arccos
    * arctg
    * sinh
    * cosh
    * tgh
    * ctgh
    * arcctg
    * ln
    * log2
    * log10
    * arcsinh
    * arccosh
    * arctgh
* Бинарные:
    * \+
    * \-
    * \*
    * /
    * **
    * log
    * mod
    
 * Бинарные над векторами:
    Вектор обозначается через []. Матрица -- вектор векторов, например
        
        [1, 2, 3] -- вектор длины 3,
        
        [[1, 2], [3, 4]] -- матрица 2x2,
        
        [[1], [2]] -- вектор столбец,
        
        [[1]] и [1] -- числа.
    
    * vAdd(,) -- сложение
    * vSub(,) -- вычитание
    * vMulNum(,) -- умножение на число
    * vGet(,) -- получение значение ветора/матрицы по индексу. Первый аргумент --
    вектор/матрица, второй -- индекс.
    * vMul(,) -- умножение
    * vTMul(,) -- тензорное умножение
 * Унарные над векторами:
    * vUSub() -- отрицание
    * vT() -- транспонирование вектора/матрицы
    
### Область определения переменных ###
В начале работы пользователю будет предложено ввести размерности
 использованных в выражении переменных. Для того, чтобы правильно
 это сделать, ввод необходимо осуществлять следующим образом:
 
 <имя переменной> <кортеж размерности переменной>
 
 Например, для того чтобы указать, что z -- вектор длины 3, необходимо
 написать следующее:
 
 z (1, 3)
 
 Если z это матрица 2 на 2, то
 
 z (2, 2)
 
 Если z -- скаляр:
 
 z ()