import unittest

from sqc.src.Tree import Tree, Rules
from sqc.src.operations import oper_symbols


class Test(unittest.TestCase):
    def test1(self):
        console_string = 'cosh(x)+cosh(y):2*cosh((x+y)/2)*cosh((x-y)/2)'
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule) > 0:
            rules.data.remove(search_rule[0])
        self.assertEquals(len(rules.data), long - 1)


    def test2(self):
        console_string = 'abs(a)+abs(b)>=abs(a+b)'
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule) > 0:
            rules.data.remove(search_rule[0])
        self.assertEquals(len(rules.data), long - 1)


    def test3(self):
        console_string = 'cosh(x+y):cosh(x)*cosh(y)+sinh(x)*sinh(y)'
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule) > 0:
            rules.data.remove(search_rule[0])
        self.assertEquals(len(rules.data), long - 1)


    def test4(self):
        console_string = 'arctg(x)>-pi/2'
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule) > 0:
            rules.data.remove(search_rule[0])
        self.assertEquals(len(rules.data), long - 1)



    def test5(self):
        console_string = 'cos(x)>=-1'
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule) > 0:
            rules.data.remove(search_rule[0])
        self.assertEquals(len(rules.data), long - 1)


    def test6(self):
        console_string = ' '
        rules = Rules()
        long = len(rules.data)
        console_string = console_string.strip('\n')
        search_rule = [rule for i, rule in enumerate(rules.data) if rule[2] == console_string]
        if len(search_rule)>0:
            rules.data.remove(search_rule[0])

        self.assertEquals(len(rules.data), long)
