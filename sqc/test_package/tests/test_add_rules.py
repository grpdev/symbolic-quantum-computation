import unittest

from sqc.src.Tree import Tree, Rules


class Test(unittest.TestCase):
    def test1(self):
        console_string = 'a<b:b>a'
        rules = Rules()
        long = len(rules.data)
        rule = rules.get_rule_from_string(console_string)
        rules.data.append((len(rules.data), rule, console_string))
        self.assertEquals(len(rules.data), long+1)

    def test2(self):
        console_string = 'cos(x)>=0'
        rules = Rules()
        long = len(rules.data)
        rule = rules.get_rule_from_string(console_string)
        rules.data.append((len(rules.data), rule, console_string))
        self.assertEquals(len(rules.data), long+1)

    def test_set_value1(self):
        input_string = 'x+y'
        tree = Tree(expression=input_string)
        arg_name, value = 'x=5'.split('=')
        tree.root.replace_by_value(tree.root, arg_name,value)
        arg_name, value = 'y=cos(0)'.split('=')
        tree.root.replace_by_value(tree.root, arg_name,value)
        tree.tree_calculate()
        output_string = tree.to_expression()
        self.assertEquals(output_string, '6.0')

    def test_set_value2(self):
        input_string = 'vAdd(x,[4,5])'
        var_shape = {'x': (1,2)}
        tree = Tree(expression=input_string, var_shapes=var_shape)
        arg_name, value = 'x=[2,3]'.split('=')
        tree.root.replace_by_value(tree.root, arg_name,value)
        tree.tree_calculate()
        output_string = tree.to_expression()
        self.assertEquals(output_string, '[6, 8]')

