import argparse

from termcolor import colored

from sqc.test_package import test
from sqc.src.interface import Interface


def run(expression, is_advanced=False):
    log = open('log.txt', 'w')

    tree = None
    while tree is None:
        if len(expression) > 0:
            ui = Interface(expression)

        if ui.tree is None:
            print('Enter expression again:')
            expression = input()

        tree = ui.tree

    step = 0
    while ui.tree is not None:
        if ui.tree.root is None:
            break

        step += 1
        print()
        print('STEP ' + str(step))
        log.write('\n STEP ' + str(step) + ' ')

        if expression.find('/0') >= 0 or expression.find('%0') >= 0:
            print(colored('Zero division. Try again.', 'red'))
            if not is_advanced:
                print("Enter 'exit' command to exit")

            answer = input()
            if len(answer) > 0:
                if answer == '0' or answer == 'exit':
                    ui.tree = None
                else:
                    expression = answer
                    ui = Interface(expression)

            step = 0
            continue

        try:
            ui.tree.tree_calculate()
            ui.aply_on_each_step()
        except ZeroDivisionError:
            print(colored('Zero division. Try again.', 'red'))
            if not is_advanced:
                print("Enter 'exit' command to exit")

            answer = input()
            if len(answer) > 0:
                if answer == '0' or answer == 'exit':
                    ui.tree = None
                else:
                    ui = Interface(answer)

            step = 0
            continue

        print('Tree:')
        print(ui.tree)

        # log.write('Tree:')
        # log.write(ui.tree)

        print(ui.show_last_k_trees(is_advanced))
        # if not is_advanced:
        #     print('Expression before applying rule on this step:' + oper_symbols[tree.action], tree.to_expression())
        # else:
        #     print('>', tree.to_expression())

        log.write(ui.tree.to_expression())

        exists_rule = ui.write_possible_rules(is_advanced)
        if exists_rule is None:
            print(colored('Error. Try again.', 'red'))
        elif not exists_rule:
            print('No possible rules. You can write new expression.')

        ui.tree, new = ui.apply_rule(log, is_advanced)
        if new:
            expression = ui.tree.to_expression()
            ui = Interface(expression)
            step = 0


def main():
    parser = argparse.ArgumentParser(description='Symbolic quantum expression shortener')
    parser.add_argument('-t', action='store_true', help='do unit test')
    parser.add_argument('--exp', help='Run program. Argument must be expression without spaces, with quotes')
    parser.add_argument('--f', help='Read expressions from your file.')
    parser.add_argument('-m', action='store_true', help='Advanced mode. Only if you know what is it.')

    args = parser.parse_args()

    if args.t:
        test.run_tests()
    elif args.exp is not None:
        run(args.exp, args.m)
    elif args.f is not None:
        ui = Interface()
        ui.read_from_file(args.f, args.m)
        if ui is not None:
            for expression in ui.data:
                run(expression, args.m)
            else:
                print(colored('File is empty', 'red'))

            ui.data.close()
    else:
        print('Enter expression:')
        expression = input()
        while len(expression) == 0:
            print('Enter expression again:')
            expression = input()

        run(expression, args.m)

if __name__ == '__main__':
    main()
