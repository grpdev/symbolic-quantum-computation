import os
import sys
import ast
from termcolor import colored

from sqc.src.Tree import Tree, Rules
from sqc.src.Constant import constants
from sqc.src.operations import Compare, oper_symbols


class Interface:
    def __init__(self, expression=None):
        self.rules = Rules()
        if expression is not None:
            variable_shapes = self.get_variable_shapes()
            self.tree = self.to_tree(expression, variable_shapes)
            self.trees_on_steps = []
            if self.tree is not None:
                self.trees_on_steps.append(self.tree)
                self.step = 0
            else:
                self.step = -1
        self.rules_use_at_each_step = []
        self.error = False
        self.data = {}
        self.max_rules = 0

    def read_from_file(self, file_path, is_advanced=False):
        while not os.path.exists(file_path):
            if file_path == "exit":
                sys.exit()
            elif file_path == '0':
                return None

            print(colored("Can not open file. Try again.", 'red'))
            if not is_advanced:
                print("Enter 'exit' command to exit")

            file_path = input()

        file = open(file_path, 'r')
        self.data = file

    @staticmethod
    def to_tree(expression, var_shapes=None):
        tree = None
        try:
            tree = Tree(var_shapes=var_shapes, expression=expression)
        except SyntaxError:
            # tree = None
            print(colored('Unexpected symbols in expression. Try again.', 'red'))

        return tree

    @staticmethod
    def get_variable_shapes():
        print('Enter names of variables and their shapes in form')
        print('<name> <shape_tuple>')
        print('End input with empty line')

        result = {}
        line = input()
        while len(line) > 0:
            if line.find(' '):
                try:
                    var, shape = line.split(' ')[0], ast.literal_eval(line.split(' ')[1])
                except SyntaxError:
                    print(colored('Unexpected symbol. Try again.', 'red'))
                else:
                    if var in result.keys():
                        print(colored('Variable already defined. Try again.', 'red'))
                    elif not isinstance(shape, tuple):
                        print(colored('Wrong shape type. Expected tuple. Try again.', 'red'))
                    elif var in constants.keys():
                        print(colored('Variable name reserved for constant. Try again.', 'red'))
                    else:
                        result[var] = shape
            else:
                print(colored('Space separated line expected. Try again.', 'red'))

            line = input()

        return result

    def write_possible_rules(self, is_advanced):
        if not is_advanced:
            print("Choose the rule or write new expression:")

        if self.step > 0:
            print('-1. Back')

        print('0. Exit')
        print()

        possible_rules = self.rules.get_possible_rules(self.tree.root, self.tree)
        num = 1
        num_rule =1
        possible_rules_ids = set([el[2] for el in possible_rules])
        for id in possible_rules_ids:
            current_rules = [(el[0], el[1]) for el in possible_rules if el[2] == id]
            rule = current_rules[0][1]
            if not is_advanced:
                print('Rule ' + str(num_rule) +':', rule[0].to_expression(), oper_symbols[rule[2]], rule[1].to_expression())
            else:
                print('>' + str(num_rule), rule[0].to_expression(), oper_symbols[rule[2]], rule[1].to_expression())

            num_rule += 1

            for new_tree, rule in current_rules:
                self.data[num] = (new_tree, rule)
                new_exp = new_tree.to_expression('yellow')

                if not is_advanced:
                    print(str(num) + '. Expression after applying this rule:', new_exp)
                else:
                    print(str(num) + '.', new_exp)

                new_exp = new_tree.to_expression()
                if new_exp.find('/0') >= 0 or new_exp.find('%0') >= 0:
                    print(colored('Zero division.', 'red'))
                    self.error = True
                num += 1

        self.max_rules = num
        print(str(self.max_rules) + '. Add new rule ')
        print(str(num+1) + '. Delete rule ')
        print(str(num+2) + '. Add rule for using at each step ')
        print(str(num+3) + '. Set value for argument ')
        if self.error:
            return None
        else:
            return num > 1

    def apply_rule(self, log, is_advanced):
        ok = False
        while not ok:
            answer = input()
            if answer is not None and len(answer) > 0:
                try:
                    answer = int(answer)
                except ValueError:
                    tree = self.to_tree(answer)
                    if tree is not None:
                        if tree.root is not None:
                            log.write('\n\n New expression: ' + answer)
                            return tree, True
                else:
                    if answer == 0:
                        print(colored('Step-by-step solution successfully saved in file log.txt', 'green'))
                        log.close()
                        return None, False
                    if answer == -1 and self.step > 0:
                        self.trees_on_steps.remove(self.trees_on_steps[self.step])
                        self.step -= 1
                        return self.trees_on_steps[self.step], False
                    elif 0 < answer < self.max_rules:
                        if not is_advanced:
                            print('Leave your note for this step: ')
                        else:
                            print('note: ')

                        note = input()
                        if note is not None and len(note) > 0:
                            log.write('  # ' + note)

                        current_tree, current_rule = self.data[answer]
                        current_tree.action_to_show = current_rule[2]
                        if current_tree.action != Compare.Gt and current_tree.action != Compare.Lt:
                            current_tree.action = current_rule[2]
                        self.trees_on_steps.append(current_tree)
                        self.step += 1
                        return current_tree, False
                    elif answer == self.max_rules:
                        print('Enter rule to add \n')
                        # print('Operations: \n * \+ \n * \- \n * sin \n * cos \n * tg \n * ctg \n * exp \n * abs \n * arcsin \n * arccos \n * arctg \n * sinh \n * cosh \n * tgh \n * ctgh \n * arcctg \n * ln \n * log2 \n * log10 \n * arcsinh \n * arccosh \n * arctgh \n ')
                        # print('Operations: \n * ** \n * log \n * mod \n * exp\n * abs \n')
                        console_string = input()
                        try:
                            if len([rule for rule in self.rules.data if rule[2] == console_string]) == 0:
                                rule = self.rules.get_rule_from_string(console_string)
                                self.rules.data.append((len(self.rules.data), rule, console_string))
                                print("New rule " + console_string + " added\n")
                            else:
                                print("Rule already exists\n")
                        except:
                            print("\nExpression is incorrect,try again \n")
                        return self.trees_on_steps[self.step], False
                    elif answer == self.max_rules+1:
                        print('Enter rule to delete')
                        console_string = input()
                        console_string = console_string.strip('\n')
                        search_rule = [rule for i, rule in enumerate(self.rules.data) if rule[2] == console_string]
                        if len(search_rule) > 0:
                            self.rules.data.remove(search_rule[0])
                            print("Deleted rule " + console_string)
                        else:
                            print("Rule not found \n")

                        return self.trees_on_steps[self.step], False
                    elif answer == self.max_rules + 2:
                        print('Enter rule to apply at each step')
                        console_string = input()
                        try:
                            rule = self.rules.get_rule_from_string(console_string)
                            self.rules_use_at_each_step.append((len(self.rules.data), rule, console_string))
                            print("Rule " + console_string + " added")
                        except:
                            print("\nExpression is incorrect,try again \n")
                        return self.trees_on_steps[self.step], False
                    elif answer == self.max_rules + 3:
                        print('Enter argument=value')
                        console_string = input()
                        try:
                            arg_name, value = console_string.split('=')
                            self.tree.root.replace_by_value(self.tree.root, arg_name, value)

                        except:
                            print("\nExpression is incorrect,try again \n")
                        return self.trees_on_steps[self.step], False
                    else:
                        print(colored('There are only ' + str(self.max_rules) + ' rules. Try again.', 'red'))

    def aply_on_each_step(self):
        if len(self.rules_use_at_each_step)>0:
            try:
                self.rules.apply_on_each_step(self.tree.root, self.tree, self.rules_use_at_each_step)
            except:
                pass

    def show_last_k_trees(self, is_advanced=False, k=3):
        if not is_advanced:
            expr = 'Expression before applying rule on this step:'
        else:
            expr = '> :'

        if self.step < k:
            k = self.step+1
        tree = self.trees_on_steps[self.step + 1 - k]
        expr += tree.to_expression()
        for i in range(1, k):
            tree = self.trees_on_steps[self.step+1-k+i]
            expr += oper_symbols[tree.action_to_show] + tree.to_expression()
        return expr
